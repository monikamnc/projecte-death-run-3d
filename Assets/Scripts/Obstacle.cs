﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

	private GameLogic logicaJoc;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.FindGameObjectWithTag ("LogicGame");
		if (go != null) {
			logicaJoc = go.GetComponent<GameLogic> ();
		} else {
			Debug.LogWarning("Eips no he trobat l'objecte LogicGane!!");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		Debug.Log ("Algú ha xocat");
		if (other.gameObject.tag == "Player") {
			Debug.Log ("Player ha xocat");
			logicaJoc.ChangeState(GameLogic.StateTypeGame.finish);
		}
	}
}
