﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour {

	public enum StateTypeGame{
		startGame,
		playing,
		finish
	}


	public float tempsJoc;
	public float tempsObstacle = 1f;

	public Transform[] posicionsObs;

	float score;
	float bestScore;

	public Camera cam;
	public GameObject panel;
	public Text Tscore;
	public Text TextScore;
	public Text TextBest;
	public GameObject obstacleX;
	public GameObject obstacle;


	public ControlCameraMovement pararPlayer;

	//StateMachine variables:
	bool isFirstTime;
	float timer;
	public StateTypeGame state;

	// Use this for initialization
	void Start () {
		ChangeState (StateTypeGame.startGame);
	}
	
	// Update is called once per frame
	void Update () {
	
		//TODO update time...
		updateStateMachine ();

		if (pauseTimer()){
			tempsJoc = Time.deltaTime+tempsJoc;
			//temporitzador.text = tempsJoc.ToString("0");
		}

		if (Input.GetKeyDown (KeyCode.A)) {
			pararPlayer.Shake();
		}
	}

	bool pauseTimer(){
		if (state == StateTypeGame.finish) {
			return false;
		}
		return true;
	}



	void compareScores(){
		if (bestScore <= score) {
			bestScore = score;
		}
	}

	public void ChangeState(StateTypeGame _state)
	{
		state = _state;
		timer = 0.0f;
		isFirstTime = true;
	}

	void updateStateMachine ()
	{
		timer += Time.deltaTime;
		
		switch (state) {
		case StateTypeGame.startGame:
			if (isFirstTime) {
				Debug.Log ("startGame");
				score = 0;
				panel.SetActive(false);
				isFirstTime = false;
			} else {
				/*if (timer > tempsGirat)
				{
					ChangeState(StateTypeGame.playing);
				}*/
				ChangeState(StateTypeGame.playing);
			}
			
			break;
		case StateTypeGame.playing:
			if (isFirstTime) {
				Debug.Log ("playing");
				isFirstTime = false;
			}
			score = cam.transform.position.z+10f;
			Tscore.text = "Score  "+ score;
			createObstacles();
			break;
		case StateTypeGame.finish:
			if (isFirstTime) {
				Debug.Log ("finish");
				isFirstTime = false;
				pararPlayer.ChangePosition(ControlCameraMovement.StatePosition.finish);
				pararPlayer.Shake();
				Tscore.enabled = false;
				compareScores();
				TextBest.text = "Best "+ bestScore.ToString("F1");

				TextScore.text = "Score  "+ score.ToString("F1");

			}
			else{
				if (timer >2.0f)
				{
					panel.SetActive(true);
				}
			}



			break;
		default:
			Debug.LogError ("updateStateMachine: Unvalid state");
			break;
		}
	}

	void createObstacles(){
		tempsObstacle -= Time.deltaTime;
		if (tempsObstacle <= 0) {
			tempsObstacle = 1.2f;

			//fer random i crear l'obstacle
			//Vector3 pos = new Vector3(obstacle.transform.position.x,obstacle.transform.position.y,cam.transform.position.z+20f);
			//GameObject obstacleCreat = Instantiate(obstacle, pos, Quaternion.identity);

			int quants = Random.Range(1,4);
			shuffle(posicionsObs);
			for(int i=0;i<quants;i++){
				Vector3 position = new Vector3(posicionsObs[i].position.x,posicionsObs[i].position.y,cam.transform.position.z+100f);
				Instantiate(obstacle, position, Quaternion.identity);
			}

			/*switch (quants){
			case 1:
				shuffle(posicionsObs);
				Vector3 position = new Vector3(posicionsObs[0].position.x,posicionsObs[0].position.y,cam.transform.position.z+20f);
				Instantiate(obstacle, position, Quaternion.identity);
				break;
			case 2:
				break;
			case 3:
				break;
			default:
				Debug.LogError ("createObstacle: Unvalid");
				break;
			}*/
			//int on = Random.Range(1,4);
			//3
			// instantiate
			//  random 
			//  3 cops

			print("He creat un obstacle");
		}
	}

	public void PlayAgain(){
		Application.LoadLevel (0);
	}

	void shuffle(Transform[] array){
		// Knuth shuffle algorithm :: courtesy of Wikipedia :)
		for (int t = 0; t < array.Length; t++ ){
			Transform tmp = array[t];
			int r = Random.Range(t, array.Length);
			array[t] = array[r];
			array[r] = tmp;
		}
	}

}
