﻿using UnityEngine;
using System.Collections;

public class Parets : MonoBehaviour {

	public GameObject Tub;
	float llargada ;

	// Use this for initialization
	void Start () {
		llargada = 49.95f * 2f;
	}
	
	// Update is called once per frame
	void Update () {

	}

		
	void OnTrigger(){
		Debug.Log ("Camara dins ontrigger");

	}

	void OnTriggerEnter(Collider other){
		Debug.Log ("Algú ha entrart");
		if (other.gameObject.tag == "Player") {
			Debug.Log ("Player dins ontriggerenter");
			Vector3 tubActual = gameObject.transform.position;
			tubActual.z += llargada;
			Instantiate(Tub, tubActual, Quaternion.identity);
		}
		
	}
	void OnTriggerExit(Collider other){
		Debug.Log ("Algú ha sortit");
		if (other.gameObject.tag == "Player") {
			Debug.Log ("Player dins ontriggerexit");
			Destroy(gameObject);
		}
		
	}

}
