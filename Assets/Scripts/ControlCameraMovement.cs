﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlCameraMovement : MonoBehaviour {

	public enum StatePosition{
		position1,
		position2,
		position3,
		position4,
		finish
	}

	//float[] posicioX;
	//float[] posicioY;
	public Transform[] posicions;

	public Vector3 moure;
	GameObject empty;

	//StateMachine variables:
	bool isFirstTime;
	float timer;
	public StatePosition state;

	private float xVibe = 0.5f;
	private float yVibe = 0.5f;
	private float zVibe = 0.5f;
	private float xRot = 0.05f;
	private float yRot = -0.05f;
	private float zRot = -0.05f;
	private float speed = 60.0f;
	private float diminish = 0.5f;
	private int numberOfShakes = 8;


	// Use this for initialization
	void Start () {
		/*posicioX [0] = 0f;
		posicioY [0] = 1f;
		posicioX [1] = -1.87f;
		posicioY [1] = 3.7f;
		posicioX [2] = 1.85f;
		posicioY [2] = 3.7f;
		posicioX [3] = -1.87f;
		posicioY [3] = -2.45f;
		posicioX [4] = 1.85f;
		posicioY [4] = -2.45f;*/
		ChangePosition (StatePosition.position1);
	}
	
	// Update is called once per frame
	void Update () {
		//TODO update time...
		if (state != StatePosition.finish) {
			UpdateCameraZ ();
			updatePositionMachine ();
		}
	}

	public void Shake()
	{
		Vibration vibration = gameObject.GetComponent<Vibration>();
		vibration.StartShaking(new Vector3(xVibe, yVibe, zVibe), new Quaternion(xRot, yRot, zRot, 1), speed, diminish, numberOfShakes);
	}

	public void ChangePosition(StatePosition _state)
	{
		state = _state;
		timer = 0.0f;
		isFirstTime = true;
	}

	void UpdateCameraZ ()
	{
		moure = gameObject.transform.position;
		moure.z += 5f;
		gameObject.transform.position = moure;
	}

	void updatePositionMachine ()
	{
		timer += Time.deltaTime;

		switch (state) {
		case StatePosition.position1:
			if (isFirstTime) {
				Debug.Log ("position1");
				moure = gameObject.transform.position;
				moure.x = posicions[0].position.x;
				moure.y = posicions[0].position.y;
				gameObject.transform.position = moure;

				isFirstTime = false;
			} else {
				if(Input.GetKeyDown(KeyCode.DownArrow)){
					ChangePosition(StatePosition.position3);
				}
				if(Input.GetKeyDown(KeyCode.RightArrow)){
					ChangePosition(StatePosition.position2);
				}
			}
			
			break;
		case StatePosition.position2:
			if (isFirstTime) {
				Debug.Log ("position2");

				moure = gameObject.transform.position;
				moure.x = posicions[1].position.x;
				moure.y = posicions[1].position.y;
				gameObject.transform.position = moure;

				isFirstTime = false;
			}else{
				if(Input.GetKeyDown(KeyCode.DownArrow)){
					ChangePosition(StatePosition.position4);
				}
				if(Input.GetKeyDown(KeyCode.LeftArrow)){
					ChangePosition(StatePosition.position1);
				}
			}
			break;
		case StatePosition.position3:
			if (isFirstTime) {
				Debug.Log ("position3");

				moure = gameObject.transform.position;
				moure.x = posicions[2].position.x;
				moure.y = posicions[2].position.y;
				gameObject.transform.position = moure;

				isFirstTime = false;
			}else{
				if(Input.GetKeyDown(KeyCode.RightArrow)){
					ChangePosition(StatePosition.position4);
				}
				if(Input.GetKeyDown(KeyCode.UpArrow)){
					ChangePosition(StatePosition.position1);
				}
			}
			
			break;
		case StatePosition.position4:
			if (isFirstTime) {
				Debug.Log ("position4");

				moure = gameObject.transform.position;
				moure.x = posicions[3].position.x;
				moure.y = posicions[3].position.y;
				gameObject.transform.position = moure;

				isFirstTime = false;
			}else{
				if(Input.GetKeyDown(KeyCode.LeftArrow)){
					ChangePosition(StatePosition.position3);
				}
				if(Input.GetKeyDown(KeyCode.UpArrow)){
					ChangePosition(StatePosition.position2);
				}
			}
			
			break;
		case StatePosition.finish:
			if (isFirstTime) {
				Debug.Log ("finishMovementCamera");

				isFirstTime = false;
			}else{

			}
			
			break;
		default:
			Debug.LogError ("updatePositionMachine: Unvalid position");
			break;
		}
	}
}
